Algoritmo "Trabalho Pratico"
// Modulo      : Design de Algoritmos
// Formador    : Nuno Miguel Fortes de Barros
// Descrição   : Programa para fazer analise de notas de uma turma
// Autor(a)    : Nuno Miguel Fortes de Barros
// Data atual  : 4/28/2024
// 42 no menu ;)

Var
   notas : Vetor [1..10, 1..3] De Inteiro // para armazenar as notas da turma
   querTerminar : Logico // para controle do termino do programa pelo usuario
   opcao : Caracter // para controle da opcao do menu

   // para armazenar valores constantes do programa
   NUMERO_DE_ALUNOS, NUMERO_DE_TESTES, NOTA_MAXIMA_TESTE : Inteiro

Procedimento Inicializar()
// Procedimento para configurar os valores de entrada iniciais do programa
Var
Inicio
   // devem ser um valores positivo (se mudar estes valores, certifique de mudar o numero de linhas e/ou colunas do vetor 'notas')
   NUMERO_DE_ALUNOS <- 10    // deve ser um valor positivo (testeado com ate 100 alunos)
   NUMERO_DE_TESTES <- 3     // deve ser um valor positivo (de 1 a 5 sao bons valores de teste)

   NOTA_MAXIMA_TESTE <- 20   // deve ser um valor positivo (5, 10, 20, 100... sao bons valores de teste)

   LancarNotas()

FimProcedimento

Procedimento MostrarMenu()
// Procedimento responsavel por apresentar o menu principal
Var
Inicio

   Escreval("Menu Principal - Gestor de Turma")
   Escreval("=================================================")
   Escreval("1. Mostrar tabela de notas da turma")
   Escreval("2. Mostrar media de um aluno...")
   Escreval("3. Mostrar medias dos alunos da turma")
   Escreval("4. Mostrar media de um teste...")
   Escreval("5. Mostrar medias dos testes da turma")
   Escreval("6. Mostrar nota minima e maxima de um aluno...")
   Escreval("7. Mostrar notas minimas e maximas dos alunos")
   Escreval("8. Mostrar nota minima e maxima de um teste...")
   Escreval("9. Mostrar notas minimas e maximas dos testes")
   Escreval("0. Mostrar info de aprovacao/reprovacao da turma")
   Escreval("")
   Escreval("-1. Terminar programa ou [ESC] em qualquer ponto!")
   Escreval("")
   Escreva("Escolha uma opcao > ")

FimProcedimento

Procedimento ExibirMensagemVoltarAoMenu()
// Procedimento auxiliar usado para pausar a execucao do programa antes de regressar ao menu principal
Var
   entrada : caractere

Inicio
   Escreval("=================================================")
   Escreval("Pressione [ENTER] para voltar ao menu principal!")
   Leia(entrada)
FimProcedimento

Procedimento MostrarInformacoes(opcao : Caracter)
// Procedimento responsavel por mostrar informacoes pedidas pelo usuario no menu principal
Var
Inicio

   LimpaTela

   Escolha opcao
   Caso "1"
      MostrarNotasTurma()
   Caso "2"
      MostrarMediaAluno()
   Caso "3"
      MostrarMediaAlunosTurma()
   Caso "4"
      MostrarMediaTeste()
   Caso "5"
      MostrarMediaTestesTurma()
   Caso "6"
      MostrarMinMaxAluno()
   Caso "7"
      MostrarMinMaxAlunosTurma()
   Caso "8"
      MostrarMinMaxTeste()
   Caso "9"
      MostrarMinMaxTestesTurma()
   Caso "0"
      MostrarReprovacao()
   Caso "42"
      OvoDePascoa()
   Caso "-1"
      querTerminar <- Verdadeiro
      Escreval("Obrigado por usar o nosso programa! ")
FimAlgoritmo
OutroCaso
   Escreval("Opcao Invalida!")
FimEscolha

ExibirMensagemVoltarAoMenu()

FimProcedimento

Procedimento LancarNotas()
// Procedimento responsavel por popular a matriz de notas
Var
   i, j : Inteiro

Inicio

   Para i De 1 Ate NUMERO_DE_ALUNOS Faca
      Para j De 1 Ate NUMERO_DE_TESTES Faca
         notas[i,j] <- Randi(NOTA_MAXIMA_TESTE)
      FimPara
   FimPara

FimProcedimento

Procedimento MostrarNotasTurma()
// Procedimento responsavel por mostras a tabela de notas da turma
Var
   i, j : Inteiro

Inicio

   Escreval("Tabela de Notas da Turma")
   Escreval("=================================================")

   Escreva("Alunos           ")
   Para i De 1 Ate NUMERO_DE_TESTES Faca
      Escreva("T",i, "   ")
   FimPara
   Escreval("")
   Para i De 1 Ate NUMERO_DE_ALUNOS Faca
      Escreva("Aluno #", i:3, " > ")
      Para j De 1 Ate NUMERO_DE_TESTES Faca
         Escreva(notas[i,j]:6)
      FimPara
      Escreval("")
   FimPara

FimProcedimento

Procedimento MostrarMediaAluno()
// Procedimento responsavel por mostrar a media das notas de um aluno
Var
   aluno, totalNotas, i : inteiro

Inicio

   Escreval("Media das Notas de um Aluno")
   Escreval("=================================================")

   Escreva("Qual aluno? (1-", NUMERO_DE_ALUNOS, ") > ")
   Leia(aluno)
   Escreval("-------------------------------------------------")

   Para i De 1 Ate NUMERO_DE_TESTES Faca
      totalNotas <- totalNotas + notas[aluno, i]
   FimPara

   Escreval("A media das notas do aluno", aluno:3, " e de ", (totalNotas/NUMERO_DE_TESTES):2:2 , " valores")

FimProcedimento

Procedimento MostrarMediaAlunosTurma()
// Procedimento responsavel por mostrar a media das notas dos alunos da turma
Var
   totalNotas, i, j : inteiro

Inicio

   Escreval("Media dos Alunos da Turma")
   Escreval("=================================================")
   Escreval("Alunos       Media")

   Para i De 1 Ate NUMERO_DE_ALUNOS Faca
      totalNotas <- 0
      Escreva("Aluno #", i:3, " > ")
      Para j De 1 Ate NUMERO_DE_TESTES Faca
         totalNotas <- totalNotas + notas[i,j]
      FimPara
      Escreval((totalNotas/NUMERO_DE_TESTES):2:2)
   FimPara

FimProcedimento

Procedimento MostrarMediaTeste()
// Procedimento responsavel por mostrar a media de notas de um teste
Var
   teste, totalNotas, i : inteiro

Inicio
   Escreval("Media das Notas de um Teste")
   Escreval("=================================================")

   Escreva("Qual teste? (1-", NUMERO_DE_TESTES, ") > ")
   Leia(teste)
   Escreval("-------------------------------------------------")

   Para i De 1 Ate NUMERO_DE_ALUNOS Faca
      totalNotas <- totalNotas + notas[i, teste]
   FimPara

   Escreval("A media das notas do teste", teste, " e de ", (totalNotas/NUMERO_DE_ALUNOS):2:2 , " valores")

FimProcedimento

Procedimento MostrarMediaTestesTurma()
// Procedimento responsavel por mostrar a media de notas dos testes da turma
Var
   totalNotas, i, j : inteiro

Inicio

   Escreval("Media dos Testes da Turma")
   Escreval("=================================================")
   Escreval("Teste        Media")


   Para j De 1 Ate NUMERO_DE_TESTES Faca
      totalNotas <- 0
      Escreva("Teste #", j:3, " > ")
      Para i De 1 Ate NUMERO_DE_ALUNOS Faca
         totalNotas <- totalNotas + notas[i,j]
      FimPara
      Escreval((totalNotas/NUMERO_DE_ALUNOS):2:2)
   FimPara

FimProcedimento

Procedimento MostrarMinMaxAluno()
// Procedimento responsavel por mostrar a nota maxima e minima de um aluno
Var
   aluno, notaMaximaAluno, notaMinimaAluno, i : inteiro

Inicio

   Escreval("Minimo e Maximo das Notas de um Aluno")
   Escreval("=================================================")

   Escreva("Qual aluno? (1-", NUMERO_DE_ALUNOS, ") > ")
   Leia(aluno)
   Escreval("-------------------------------------------------")

   notaMinimaAluno <- NOTA_MAXIMA_TESTE + 1
   notaMaximaAluno <- -1

   Para i De 1 Ate NUMERO_DE_TESTES Faca
      Se notas[aluno, i] > notaMaximaAluno Entao
         notaMaximaAluno <- notas[aluno, i]
      FimSe

      Se notas[aluno, i] < notaMinimaAluno Entao
         notaMinimaAluno <- notas[aluno, i]
      FimSe
   FimPara

   Escreval("A nota maxima do aluno ", aluno:3, " e de ", notaMaximaAluno:2, " valores")
   Escreval("A nota minima do aluno ", aluno:3, " e de ", notaMinimaAluno:2, " valores")

FimProcedimento

Procedimento MostrarMinMaxAlunosTurma()
// Procedimento responsavel por mostrar a nota maxima e minima dos alunos da turma
Var
   notaMaximaAluno, notaMinimaAluno, i, j : inteiro

Inicio

   Escreval("Minimos e Maximos dos Alunos da Turma")
   Escreval("=================================================")
   Escreval("Aluno            Min   Max")

   Para i De 1 Ate NUMERO_DE_ALUNOS Faca
      notaMinimaAluno <- NOTA_MAXIMA_TESTE + 1
      notaMaximaAluno <- -1

      Para j De 1 Ate NUMERO_DE_TESTES Faca
         Se notas[i, j] > notaMaximaAluno Entao
            notaMaximaAluno <- notas[i, j]
         FimSe

         Se notas[i, j] < notaMinimaAluno Entao
            notaMinimaAluno <- notas[i, j]
         FimSe
      FimPara

      Escreva("Aluno #", i:3, " > ")
      Escreval(notaMinimaAluno:6, notaMaximaAluno:6)
   FimPara

FimProcedimento

Procedimento MostrarMinMaxTeste()
// Procedimento responsavel por mostrar a nota maxima e minima de um teste
Var
   teste, notaMaximaTeste, notaMinimaTeste, i : inteiro

Inicio

   Escreval("Minimo e Maximo dos Testes da Turma")
   Escreval("=================================================")

   Escreva("Qual teste? (1-", NUMERO_DE_TESTES, ") > ")
   Leia(teste)
   Escreval("-------------------------------------------------")

   notaMinimaTeste <- NOTA_MAXIMA_TESTE + 1
   notaMaximaTeste <- -1

   Para i De 1 Ate NUMERO_DE_ALUNOS Faca
      Se notas[i, teste] > notaMaximaTeste Entao
         notaMaximaTeste <- notas[i, teste]
      FimSe

      Se notas[i, teste] < notaMinimaTeste Entao
         notaMinimaTeste <- notas[i, teste]
      FimSe
   FimPara

   Escreval("A nota maxima do teste ", teste:3, " e de ", notaMaximaTeste:2, " valores")
   Escreval("A nota minima do teste ", teste:3, " e de ", notaMinimaTeste:2, " valores")

FimProcedimento

Procedimento MostrarMinMaxTestesTurma()
// Procedimento responsavel por mostrar a nota maxima e minima dos testes da turma
Var
   notaMaximaTeste, notaMinimaTeste, i, j : inteiro
Inicio

   Escreval("Minimos e Maximos dos Testes da Turma")
   Escreval("=================================================")
   Escreval("Teste            Min   Max")

   Para j de 1 ate NUMERO_DE_TESTES Faca
      notaMinimaTeste <- NOTA_MAXIMA_TESTE + 1
      notaMaximaTeste <- -1

      Para i De 1 Ate NUMERO_DE_ALUNOS Faca
         Se notas[i, j] > notaMaximaTeste Entao
            notaMaximaTeste <- notas[i, j]
         FimSe

         Se notas[i, j] < notaMinimaTeste Entao
            notaMinimaTeste <- notas[i, j]
         FimSe
      FimPara

      Escreva("Teste #", j:3, " > ")
      Escreval(notaMinimaTeste:6, notaMaximaTeste:6)
   FimPara

FimProcedimento

Procedimento MostrarReprovacao()
// Procedimento responsavel por mostrar estatisticas de reprovacao da turma
Var
   totalNotas, i, j, aprovados : inteiro

Inicio

   Escreval("Aprovacao/Reprovacao dos Alunos da Turma")
   Escreval("=================================================")

   Para i De 1 Ate NUMERO_DE_ALUNOS Faca
      totalNotas <- 0
      Para j De 1 Ate NUMERO_DE_TESTES Faca
         totalNotas <- totalNotas + notas[i,j]
      FimPara
      Se totalNotas/NUMERO_DE_TESTES > NOTA_MAXIMA_TESTE/2 Entao
         aprovados <- aprovados + 1
      FimSe
   FimPara

   Escreval("# Aprovados:", aprovados:2)
   Escreval("# Reprovados:", NUMERO_DE_ALUNOS-aprovados:2)
   Escreval("-------------------------------------------------")
   Escreval("% Aprovados:", ((aprovados/NUMERO_DE_ALUNOS)*100):6:2, "%")
   Escreval("% Reprovados:", (((NUMERO_DE_ALUNOS-aprovados)/NUMERO_DE_ALUNOS)*100):6:2, "%")

FimProcedimento

// Easter Egg :D
Procedimento OvoDePascoa()()
Var
Inicio

   Escreval("Answer to the Ultimate Question of Life,")
   Escreval("the Universe,")
   Escreval("and Everything!")
   Escreval("                           d8888   .d8888b.  ")
   Escreval("                          d8P888  d88P  Y88b ")
   Escreval("                         d8P 888         888 ")
   Escreval("                        d8P  888       .d88P ")
   Escreval("                       d88   888   .od8888P  ")
   Escreval("                       8888888888 d888P      ")
   Escreval("                             888  8888       ")
   Escreval("                             888  8888888888 ")

FimProcedimento

// Inicio do algoritmo principal
Inicio

   Inicializar()

   Enquanto Nao querTerminar Faca
      LimpaTela
      MostrarMenu()
      Leia(opcao)
      MostrarInformacoes(opcao)
   FimEnquanto

FimAlgoritmo
