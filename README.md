# 🎓 Trabalho Prático: Análise de Notas de uma Turma

[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/noxfortes/analise-de-notas/-/blob/master/LICENSE)

## 📘 Sobre Este Repositório
Este repositório contém a solução proposta pelo formador para avaliação prática. Ele serve como referência e estudo para futuras turmas e interessados.
Foi proposto como um exercício prático para a Unidade Formativa de Design de Algoritmos. Ele analisa as notas de uma turma e oferece diversas funcionalidades para gerenciar e analisar as notas dos alunos.

## 📅 Data de Desenvolvimento
28 de abril de 2024

## 🛠️ Linguagem e Software
- Linguagem: Portugol (Portugues Estruturado)
- Software: Visualg 3.0.7

## 📋 Funcionalidades
O programa oferece diversas funcionalidades para gerenciar e analisar as notas dos alunos:

- Mostrar Tabela de Notas: Exibe as notas de todos os alunos.
- Mostrar Média do Aluno: Calcula e exibe a média das notas de um aluno selecionado.
- Mostrar Médias da Turma: Exibe as médias das notas de todos os alunos.
- Mostrar Média do Teste: Calcula e exibe a média das notas de um teste selecionado.
- Mostrar Médias dos Testes: Exibe as médias das notas de todos os testes.
- Mostrar Notas Mínimas/Máximas do Aluno: Exibe as notas mínimas e máximas de um aluno selecionado.
- Mostrar Notas Mínimas/Máximas da Turma: Exibe as notas mínimas e máximas de todos os alunos.
- Mostrar Notas Mínimas/Máximas do Teste: Exibe as notas mínimas e máximas de um teste selecionado.
- Mostrar Notas Mínimas/Máximas do Teste (Turma): Exibe as notas mínimas e máximas de todos os testes.
- Mostrar Informações de Aprovação/Reprovação: Exibe estatísticas sobre o número e a porcentagem de alunos aprovados e reprovados.
- Easter Egg: Inclui um Easter egg divertido para os usuários descobrirem.

## 🏃🏽‍♂️ Executando o Programa
Para executar o programa, siga estes passos:

1. Instale o [Visualg](https://sourceforge.net/projects/visualg30/)
2. Execute o arquivo `.alg`
3. O menu principal será exibido
4. Escolha uma opção digitando o número correspondente
5. A operação selecionada será executada, e os resultados serão exibidos
6. Pressione [ENTER] para retornar ao menu principal após visualizar os resultados
7. Para sair do programa, escolha a opção -1 ou pressione [ESC] a qualquer momento

## 🎯 Objetivo
Desenvolver um sistema para análise das notas de três testes de uma turma, utilizando funções para facilitar o teste do programa, além dos conceitos aprendidos na disciplina de Design de Algoritmos.

## 📋 Requisitos Funcionais
1. 📊 Armazenar as notas dos três testes de cada aluno de uma turma.
2. 📈 Calcular a média das notas de cada aluno.
3. 🔍 Identificar a maior e a menor nota de cada aluno.
4. 📉 Calcular a média das notas de cada teste da turma.
5. 🏆 Identificar a maior e a menor nota de cada teste da turma.
6. ✔️ Contar o número de alunos aprovados (média maior ou igual a 5) e reprovados (média menor que 5) em cada teste.
7. 📊 Calcular a percentagem de alunos aprovados e reprovados em cada teste.
8. 👀 Permitir visualização de uma tabela com todas as notas de todos os alunos.

## 🛠️ Requisitos Técnicos
1. 💻 Utilizar a linguagem de programação Portugol Estruturado.
2. 🧩 Utilizar funções para facilitar a atribuição aleatória e a população das estruturas de dados com notas.
3. 🔄 Utilizar estruturas condicionais e de repetição para controle de fluxo.
4. 🗃️ Utilizar variáveis compostas homogêneas para armazenamento das notas de cada aluno.
5. 📜 Utilizar subalgoritmos para facilitar a organização do código.
6. 🚨 Implementar tratamento de erros para situações como notas inválidas.

## 📦 Entrega
1. **💾 Código fonte do programa:** Comprimido (em .zip ou .7z), em um ficheiro com o nome `<Nome Sobrenome> - TPDA`, devidamente comentado e organizado.
2. **📄 Relatório estruturado:** Explicando a lógica por trás dos algoritmos utilizados, as limitações e desafios encontrados e as soluções adotadas. Deve ter no máximo 3 páginas.
3. **🗣️ Apresentação oral:** Demonstrando o funcionamento do sistema e discutindo as decisões de design tomadas durante o desenvolvimento. Deverá ser realizada em 5 minutos pelo formando, seguidos de 5 minutos de apreciação e perguntas do formador.
4. **📅 Prazo de entrega:** Via e-mail para noxfortes@gmail.com com o assunto “Entrega de Trabalho Prático de Design de Algoritmos”, até o dia 19 de abril às 12h00 (meio-dia). Entregas realizadas depois do tempo limite serão gravemente penalizadas.

### 📅 Datas de Apresentação
- **📆 Primeiro grupo:** 19 de abril de 2024
- **📆 Segundo grupo:** 22 de abril de 2024

## 📝 Tarefas Futuras
- [ ] Melhorar a Validação dos Dados: Aprimorar a validação dos dados de entrada para índices de aluno e teste.
- [ ] Adicionar Nova Opção de Menu - Perfil do Aluno: Incluir uma opção para mostrar um perfil detalhado de um aluno selecionado.
- [ ] Adicionar Nova Opção de Menu - Relatório da Turma: Incluir uma opção para mostrar um relatório abrangente da turma.

## 📝 Como Contribuir

    1. 🍴 Faça um fork deste repositório.
    2. 🌿 Crie uma branch para a sua feature (git checkout -b feature/AmazingFeature).
    3. 💬 Commit suas alterações (git commit -m 'Add some AmazingFeature').
    4. 🚀 Faça um push para a branch (git push origin feature/AmazingFeature).
    5. 📬 Abra um pull request.

## 📄 Licença

Este projeto está licenciado sob a Licença MIT. Consulte o arquivo [LICENSE](LICENSE) para obter mais detalhes.